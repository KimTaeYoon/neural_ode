using DifferentialEquations
using Plots

function Lorenz_system(du, u, p, t)
    x, y, z = u
    alpha, beta, gamma = p
    du[1] = alpha*(y-x)
    du[2] = x*(gamma-z)-y
    du[3] = x*y - beta*z
end

u0 = [1.0, 1.0, 1.0] #initial value
tspan = (0.0, 20.0)
p= [10, 8/3, 28] #parameters
prob = ODEProblem(Lorenz_system, u0, tspan, p)
t_step=0.1
# function lotka_volterra(du,u,p,t)
#     x, y = u
#     α, β, δ, γ = p
#     du[1] = dx = α*x - β*x*y
#     du[2] = dy = -δ*y + γ*x*y
#   end
#   u0 = [1.0,1.0]
#   tspan = (0.0,10.0)
#   p = [1.5,1.0,3.0,1.0]
#   prob = ODEProblem(lotka_volterra,u0,tspan,p)

sol =solve(prob,Tsit5(),saveat=t_step)
A=sol[1,:];

# plot(sol[1,:])
# t=0:0.01:20.0
# scatter!(t,A)

###########################
using Flux, DiffEqFlux
p1=[9,2.1,25]
params = Flux.params(p1)

function predict_rd() # one-layer "neural network"
    solve(prob,Tsit5(),p=p1,saveat=t_step)[1,:] #override with new parameters??
end

# loss_rd() = sum(abs2,x-1 for x in predict_rd()) #loss function

function loss_rd()
    sum(abs2,predict_rd()-A)
end

data = Iterators.repeated((),400) # 100 epoch
opt = ADAM(0.04) #optimizer
cb = function () #callback function to observe training
    display(loss_rd())
    # using 'remake' to re-create our 'prob' with current parameters 'p'
    display(plot(solve(remake(prob,p=p1),Tsit5(),saveat=t_step)[1,:],ylim=(0.6)))
    display(plot!(sol[1,:]))
end

# Display the ODE with the initial parameter values.
cb()

Flux.train!(loss_rd, params, data, opt, cb=cb)

################  Neural ODE   ############################

using DifferentialEquations
using Plots
using Flux, DiffEqFlux
# function trueODEfunc(du,u,p,t)
#     true_A = [-0.1 2.0; -2.0 -0.1]
#     du .=((u.^3)'true_A)'
# end

function Lorenz_system(du, u, p, t)
    x, y, z = u
    alpha, beta, gamma = p
    du[1] = alpha*(y-x)
    du[2] = x*(gamma-z)-y
    du[3] = x*y - beta*z
end

u0 = Float32[1.; 1.; 1.] #initial condition
t_step=0.1
tspan = (0.0f0,20.0f0)
datasize=floor(Int16,(tspan[2]-tspan[1])/t_step)
t = range(tspan[1],tspan[2],length=datasize)
p= [10, 8/3, 28] #parameters

# t = range(tspan[1],tspan[2],length=datasize)
prob = ODEProblem(Lorenz_system,u0,tspan,p)
ode_data = Array(solve(prob,Tsit5(),saveat=t))

dudt = Chain(x -> x.^3, Dense(3,50,tanh),Dense(50,3)) #network structure
n_ode = NeuralODE(dudt, tspan, Tsit5(),saveat=t,reltol=1e-7,abstol=1e-9)
ps = Flux.params(n_ode)

pred = n_ode(u0) # Get the prediction using the correct initial condition
scatter(t,ode_data[1,:],label="data")
scatter!(t,pred[1,:],label="prediction")

function predict_n_ode()
    n_ode(u0)
end
loss_n_ode() = sum(abs2,ode_data .- predict_n_ode())

data = Iterators.repeated((), 100)
opt = ADAM(0.1)
cb = function () #callback function to observe training
  display(loss_n_ode())
  # plot current prediction against data
  cur_pred = predict_n_ode()
  pl = scatter(t,ode_data[1,:],label="data")
  scatter!(pl,t,cur_pred[1,:],label="prediction")
  display(plot(pl))
end

# Display the ODE with the initial parameter values.
cb()

Flux.train!(loss_n_ode, ps, data, opt, cb = cb)